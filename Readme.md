# [Watch Together SDK](https://documentation.sceenic.co/)
Watch Together video-chat is a general-purpose video chatting infrastructure 
that is aimed to provide high-quality Audio and Video functionality
that will allow you to create and provide engaging experiences in your application for your customers.  

To register to our service please contact us on [Support@sceenic.co](mailto:Support@sceenic.co).

If you already have credentials to the private area, login and retrieve your `API_KEY` and `API_SECRET` - [Private area](https://media.sceenic.co)
Have a look at the [Watch-Together overview](https://documentation.sceenic.co/sscale-confluence-watch-together-overview), once done, you can [set up your Authentication server](https://documentation.sceenic.co/watch-together-sdk/sscale-confluence-overview#customer-authentication-server) and start working.

**( * ) SDKs are accessible only from the private area**

Need technical support? contact us at [Support@sceenic.co](mailto:Support@sceenic.co)

# Tutorial
1. Retrieve `NPM_TOKEN` from private area
2. Copy `.npmrc-example` to `.npmrc` and replace `YOUR_AUTH_NPM_TOKEN` with `NPM_TOKEN` obtained in a previous step
3. Run `npm i`
4. Run `npm build` (only for linux based systems)
5. Run `npx http-server`

For joining to one room you need a session token.
After that you should open `http://127.0.0.1:8080` in two browser tab, fill form and press join for each. 

## Documentation

Have a look at our official documentation site [Sceenic - WatchTogether](https://documentation.sceenic.co)

To get our sample working, go to our [WEB - tutorial](https://documentation.sceenic.co/watch-together-sdk/sscale-confluence-tutorials/sscale-confluence-web/web-javascript-reactjs2) and follow the instructions.

Read more about our SDK methods in [WEB SDK documentation](https://documentation.sceenic.co/watch-together-sdk/sscale-confluence-api-references/sscale-confluence-web-sdk-reference)
