/**
  * Watch Together Session
	*
	* WT.Session.onConnected - Method for setting up session when it is ready
	* @param {onConnectedCallback} callback - Callback fired when session is ready
	* @callback onConnectedCallback - Callback function containing an array of participants in the session
	* @param {Array} Participants
	*
	* WT.SessionListeners.onStreamCreated - Method will allow you to get information about new participants
	* @param {onStreamCreated} callback - Callback fired everytime when participant stream is ready
	* @callback onStreamCreated - Callback function containing object with participant information
	* @param {Object} ParticipantInfo
	*
	* WT.ParticipantListeners.onParticipantLeft - Method will allow you to get information about participant which left the session
	* @param {onParticipantLeftCallback} callback - Callback fired everytime when participant leaves the session
	* @callback onParticipantLeftCallback
	* @param {String} ParticipantName
	*
	* WT.Session.disconnect - Method will allow you to disconnect from the Session
	*
	* WT.Session.connect - Method will allow you to connect to the Session
	* @param {Object} SessionSettings - Information for creating the session
	* @param SessionSettings.sessionToken - Session token
	* @param SessionSettings.wsUrl - WebSocket Url
	* @param SessionSettings.sessionId - The identifier of the session
	* @param {Object} UserInformation - Information about user
	* @param UserInformation.displayed_name - User ID
	*
	* WT.ErrorListeners.onSessionError - Method will allow you to handle session errors
	* @param {onSessionErrorCallback} callback - Callback fired everytime when arises error with session
	* @callback onSessionErrorCallback
	* @param {Object} ErrorObject - Information about session error
	* @param code - Error code
	* @param message - Error message
	* @param requestTime - Error time in unix timestamp
*/
let cameraState = true;
let micState = true;
let videoWidth = 320;
let videoHeight = 240;
let frameRate = 12;

function toggleVideo(){
	if(cameraState){
		WTSDK.Participant.disableVideo();
		cameraState = false;
		const video = document.getElementById('vide_control');
		video.classList.remove('vcam_on');
		video.classList.add('vcam_off');
	}else {
		WTSDK.Participant.enableVideo();
		cameraState = true;
		const video = document.getElementById('vide_control');
		video.classList.remove('vcam_off');
		video.classList.add('vcam_on');
	}
}
function toggleAudio() {
	if(micState){
		WTSDK.Participant.disableAudio();
		micState = false;
		const video = document.getElementById('audio_control');
		video.classList.remove('mic_on');
		video.classList.add('mic_off');
	}else {
		WTSDK.Participant.enableAudio();
		micState = true;
		const video = document.getElementById('audio_control');
		video.classList.remove('mic_off');
		video.classList.add('mic_on');
	}
}

WTSDK.SessionListeners.onConnected(() => {

	document.getElementById('session_container').style = `display: flex; flex-direction: column; align-items: center;`;
	document.getElementById('config_dialog').style.display = 'none';

	const videos = document.getElementsByTagName('video')

	//On participant connect to the Session
	WTSDK.ParticipantListeners.onParticipantSpeaking(() => {
		// On participant speaking event
	});
	WTSDK.ParticipantListeners.onParticipantStopSpeaking(() => {
		// On participant stop speaking event
	});

	WTSDK.ReconnectListeners.onLocalConnectionResumed(() => {
		Array.from(videos).forEach(video => {
			video.parentElement.remove();
		})
	})

	WTSDK.SessionListeners.onStreamCreated(participant => {
		const { local, stream, participantId } = participant;

		const node = document.getElementById(participantId);

		if(node) {
			node.srcObject = participant.stream;
		} else {
			const div = document.createElement("div")
			div.setAttribute("class", "video-container");

			const video = document.createElement("video");
			video.id = participantId;
			video.className = local ? "local" : "";
			video.autoplay = true;
			video.muted = local;
			video.srcObject = stream;
			video.playsInline = true;
			video.disablePictureInPicture = true;

			div.appendChild(video)

			const container = document.querySelector("#gallery");
			container.appendChild(div);
			recalculateLayout();
		}
	});

	WTSDK.ParticipantListeners.onParticipantLeft(({participantId}) => {
		const el = document.getElementById(participantId);
		el.parentElement.remove();
		recalculateLayout();
	});
});

WTSDK.ErrorsListeners.onSessionError((e) => {
	console.log('handling errors');

	//Error codes: https://documentation.sceenic.co/watch-together-sdk/sscale-confluence-api-references/sscale-confluence-web-sdk-reference/sscale-confluence-errorlisteners-websdk#session-error-codes
	console.log(e.code);
	console.log(e.message);
});

function connect() {
	let displayed_name = document.getElementById('display_name').value
	let streamingToken = document.getElementById('session_token').value

	if(!displayed_name || !streamingToken)
		alert('Display name and token is empty or invalid! Please check and try again!')

	WTSDK.Session.connect(
		streamingToken,
		displayed_name
	);
	// document.getElementById('session_container').style.display = 'flex';
	// document.getElementById('dialog_container').style.display = 'none';
}

function disconnect() {
	WTSDK.Session.disconnect();
	document.getElementById("gallery").innerHTML = "";

	document.getElementById('session_container').style.display = 'none';
	document.getElementById('config_dialog').style = `display: flex; flex-direction: column;`;
}

function changeVolume(val) {
	const videos = Array.prototype.slice.call(document.getElementsByTagName('video') || []);
	videos.forEach(v => {
		if(!v.muted) {
			v.volume = val / 100;
		}
	})
}

function changeWidth(width){
	videoWidth = width;
	document.getElementById('width-label').innerHTML = width;
}
function changeHeight(height){
	videoHeight = height;
	document.getElementById('height-label').innerHTML = height;
}
function applyQuality () {
	WTSDK.Participant.setMediaQuality({videoWidth: videoWidth, videoHeight: videoHeight, frameRate: 12})
}
function setVideoQuality(width, height, frameRate ) {
	let constraints = {};
	if(width)
		constraints.videoWidth = width
	if(height)
		constraints.videoHeight = height
	if(frameRate)
		constraints.frameRate = frameRate
	WTSDK.Participant.setMediaQuality(constraints)
}

function recalculateLayout() {
	const gallery = document.getElementById('gallery');
	const aspectRatio = 16 / 9;
	const screenWidth = document.body.getBoundingClientRect().width;
	const screenHeight = document.body.getBoundingClientRect().height - 100;
	const videoCount = document.getElementsByTagName('video').length;

	// or use this nice lib: https://github.com/fzembow/rect-scaler
	function calculateLayout(
		containerWidth,
		containerHeight,
		videoCount,
		aspectRatio
	) {
		let bestLayout = {
			area: 0,
			cols: 0,
			rows: 0,
			width: 0,
			height: 0
		};

		// brute-force search layout where video occupy the largest area of the container
		for (let cols = 1; cols <= videoCount; cols++) {
			const rows = Math.ceil(videoCount / cols);
			const hScale = containerWidth / (cols * aspectRatio);
			const vScale = containerHeight / rows;
			let width;
			let height;
			if (hScale <= vScale) {
				width = Math.floor(containerWidth / cols);
				height = Math.floor(width / aspectRatio);
			} else {
				height = Math.floor(containerHeight / rows);
				width = Math.floor(height * aspectRatio);
			}
			const area = width * height;
			if (area > bestLayout.area) {
				bestLayout = {
					area,
					width,
					height,
					rows,
					cols
				};
			}
		}
		return bestLayout;
	}

	const { width, height, cols } = calculateLayout(
		screenWidth,
		screenHeight,
		videoCount,
		aspectRatio
	);

	gallery.style.setProperty("max-width", width * cols + "px");

	const c = document.getElementsByClassName('video-container');
	for(let i = 0; i < c.length; i++) {
		c[i].style.setProperty('width', width + 'px');
		c[i].style.setProperty('height', height + 'px');
	}

}

window.addEventListener("resize", recalculateLayout);
recalculateLayout();


