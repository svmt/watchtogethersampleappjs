(() => {
    const tourConnect = new Shepherd.Tour({
        useModalOverlay: true,
        defaultStepOptions: {
        classes: 'shadow-md bg-purple-dark',
        scrollTo: true
        }
    });


    const tourConnectSteps = [
        {
            id: 'display_name',
            text: `Name is a required parameter for 
                <a href="https://documentation.sceenic.co/watch-together-sdk/sscale-confluence-api-references/sscale-confluence-web-sdk-reference/wt-session/wtsession.connect-stoken-pname-uc-m" target="_blank">WTSession.connect</a>
            `,
            element: '#display_name',
        },
        {
            id: 'session_token',
            text: `SessionToken is a equired parameter for <a href="https://documentation.sceenic.co/watch-together-sdk/sscale-confluence-api-references/sscale-confluence-web-sdk-reference/wt-session/wtsession.connect-stoken-pname-uc-m" target="_blank">WTSession.connect</a>.
                <br />
                Have a look at the <a href="https://documentation.sceenic.co/watch-together-sdk/sscale-confluence-tutorials/web/authentication" target="_blank">Authentication overview</a>
            `,
            element: '#session_token',
        },
        {
            id: 'session_join_btn',
            text: `Have a look at the 
                <a href="https://documentation.sceenic.co/watch-together-sdk/sscale-confluence-tutorials/web/sample-application/connecting-to-a-session" target="_blank">Connecting to a Session</a> 
                and 
                <a href="https://documentation.sceenic.co/watch-together-sdk/sscale-confluence-api-references/sscale-confluence-web-sdk-reference/wt-session/wtsession.connect-stoken-pname-uc-m" target="_blank">WTSession.connect function</a>
            `,
            element: '#session_join_btn',
        },
    ];

    tourConnectSteps.forEach(item => {
        tourConnect.addStep({
            id: item.id,
            text: item.text,
            attachTo: {
                element: item.element,
                on: 'right'
            },
            classes: 'example-step-extra-class',
            buttons: [
            {
                text: 'Next',
                action: tourConnect.next
            }
            ]
        });
    });

    // tourSession
    const tourSession = new Shepherd.Tour({
        useModalOverlay: true,
        defaultStepOptions: {
        classes: 'shadow-md bg-purple-dark',
        scrollTo: true
        }
    });


    const tourSessionSteps = [
        {
            id: 'local',
            text: `Here is all about
                <a href="https://documentation.sceenic.co/watch-together-sdk/sscale-confluence-api-references/sscale-confluence-web-sdk-reference/wt-session" target="_blank">WT Session</a>
                and
                <a href="https://documentation.sceenic.co/watch-together-sdk/sscale-confluence-api-references/sscale-confluence-web-sdk-reference/sscale-confluence-participant-websdk" target="_blank">WT Participant</a>
            `,
            element: '.local',
        },
        {
            id: 'disconnect-button',
            text: `Here is all about
            <a href="https://documentation.sceenic.co/watch-together-sdk/sscale-confluence-api-references/sscale-confluence-web-sdk-reference/wt-session" target="_blank">WT Session</a>
            and
            <a href="https://documentation.sceenic.co/watch-together-sdk/sscale-confluence-api-references/sscale-confluence-web-sdk-reference/wt-session/wtsession.disconnect" target="_blank">WTSession.disconnect()</a>
        `,
            element: '.disconnect-button',
        },
        {
            id: 'vide_control',
            text: 'Have a look at the <a href="https://documentation.sceenic.co/watch-together-sdk/sscale-confluence-tutorials/web/sample-application/how-to-turn-on-and-off-video-and-audio" target="_blank">How to turn on and off video and audio</a>',
            element: '#vide_control',
        },
        {
            id: 'audio_control',
            text: 'Have a look at the <a href="https://documentation.sceenic.co/watch-together-sdk/sscale-confluence-tutorials/web/sample-application/how-to-turn-on-and-off-video-and-audio" target="_blank">How to turn on and off video and audio</a>',
            element: '#audio_control',
        },
        {
            id: 'quality-button',
            text: 'Here is information about <a href="https://documentation.sceenic.co/watch-together-sdk/sscale-confluence-tutorials/web/sample-application/how-to-change-video-quality" target="_blank">Set*Quality</a> functions',
            element: '.quality-button',
        },
    ];

    tourSessionSteps.forEach(item => {
        tourSession.addStep({
            id: item.id,
            text: item.text,
            attachTo: {
                element: item.element,
                on: 'top'
            },
            classes: 'example-step-extra-class',
            buttons: [
            {
                text: 'Next',
                action: tourSession.next
            }
            ]
        });
    });

    const tourBtn = document.getElementById('tour-btn');

    tourBtn.onclick = () => {
        if (document.getElementsByClassName('local')[0]) {
            tourSession.cancel();
            tourSession.start();
        }
        else {
            tourConnect.cancel();
            tourConnect.start();
        }
    }

    const searchParams = new URLSearchParams(window.location.search);
    const isDocsMode = searchParams.get("mode") === "docs";

    if (tourBtn && isDocsMode) {
        tourBtn.style.display = 'flex';
    }
})();